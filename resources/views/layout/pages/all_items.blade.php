@extends('layout.base')

@section('title', 'All Item page')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card m-2">
                <div class="card-header">
                    All Items
                </div>
                <div class="list-group">
                    @foreach($items as $item)
                        <a href="{{route('item', ['id' => $item->id])}}" class="list-group-item list-group-item-action">
                            {{ $item->name }}
                            <img src="{{url('/item_images/'.$item->images()->first()->file_name)}}" class="img-thumbnail w-25 pull-right" />
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card m-2">
                <div class="card-header">
                    Add Item
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>'add.item', 'files'=>true]) !!}
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('Name:') !!}
                            {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter name']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('base_price') ? 'has-error' : '' }}">
                            {!! Form::label('Base price:') !!}
                            {!! Form::number('base_price', old('base_price'), ['class'=>'form-control', 'placeholder'=>'Enter base price']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('category_id.*') ? 'has-error' : '' }}">
                            {!! Form::label('Category:') !!}
                            {!! Form::select('category_id',$allCategories, old('category_id'), ['multiple'=>'multiple', 'name'=>'category_id[]', 'class'=>'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('category_id') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('images.*') ? 'has-error' : '' }}">
                            {!! Form::label('Images:') !!}
                            {!! Form::file('images[]', array('multiple'=>true,'accept'=>'image/*')) !!}
                            <span class="text-danger">{{ $errors->first('images') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Add New</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
