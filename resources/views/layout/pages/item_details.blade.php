@extends('layout.base')

@section('title', 'Category detail page')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <h2>{{ucfirst($item->name)}} item detail</h2>
        </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">Price Check</button>
        </div>
    </div>
    <div class="col-md-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @elseif ($message = Session::get('danger'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card m-2">
                <div class="card-header">
                    Item Images
                </div>
                <div class="row p-2">
                    @foreach($item->images as $image)
                        <div class="col-lg-4 col-md-12 mb-4 mb-lg-0 m-3 border ">
                           <img src="{{url('/item_images/'.$image->file_name)}}" class="w-100" />
                        </div>
                        <a href="#" onclick="removeImage({{$image->id}});" class="mt-3"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card m-2">
                <div class="card-header">
                    Edit Item Detail
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>'edit.item', 'files'=>true]) !!}
                    {{ Form::hidden('item_id', $item->id) }}
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('Name:') !!}
                            {!! Form::text('name', old('name', $item->name), ['class'=>'form-control', 'placeholder'=>'Enter name']) !!}
                        </div>
                        <div class="form-group {{ $errors->has('base_price') ? 'has-error' : '' }}">
                            {!! Form::label('Base price:') !!}
                            {!! Form::number('base_price', old('base_price', $item->base_price), ['class'=>'form-control', 'placeholder'=>'Enter base price']) !!}
                            <span class="text-danger">{{ $errors->first('base_price') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                            {!! Form::label('Category:') !!}
                            {!! Form::select('category_id',$allCategories, old('category_id', $item->categories()->pluck('category_id')), ['multiple'=>'multiple', 'name'=>'category_id[]', 'class'=>'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('category_id') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                            {!! Form::label('Images:') !!}
                            {!! Form::file('images[]', array('multiple'=>true,'accept'=>'image/*')) !!}
                            <span class="text-danger">{{ $errors->first('images') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Edit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card m-2">
            <div class="card-header">
                Item Price Tier
            </div>
            <div class="card-body">
                <div class="row m-2">
                    <div class="col-md-4">
                        <span class="text-danger">{{ $errors->first('min_quantity') }}</span>
                    </div>
                    <div class="col-md-4">
                        <span class="text-danger">{{ $errors->first('max_quantity') }}</span>
                    </div>
                    <div class="col-md-4">
                        <span class="text-danger">{{ $errors->first('unit_price') }}</span>
                    </div>
                </div>

                @foreach($item->prices as $price)
                    <div class="row m-2">
                    {!! Form::open(['route'=>'edit.item.price', 'class'=>"form-inline"]) !!}
                        {{ Form::hidden('item_price_id', $price->id) }}
                        <div class="form-group {{ $errors->has('min_quantity') ? 'has-error' : '' }}">
                            {!! Form::label('Min quantity:') !!}
                            {!! Form::number('min_quantity', old('min_quantity', $price->min_quantity), ['class'=>'form-control', 'placeholder'=>'Enter min quantity']) !!}
                        </div>
                        <div class="form-group {{ $errors->has('max_quantity') ? 'has-error' : '' }}">
                            {!! Form::label('Max quantity:') !!}
                            {!! Form::number('max_quantity', old('max_quantity',$price->max_quantity), ['class'=>'form-control', 'placeholder'=>'Enter max quantity']) !!}
                        </div>
                        <div class="form-group {{ $errors->has('unit_price') ? 'has-error' : '' }}">
                            {!! Form::label('Unit price:') !!}
                            {!! Form::number('unit_price', old('unit_price',$price->unit_price), ['class'=>'form-control', 'placeholder'=>'Enter unit price']) !!}
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                @endforeach
                <div class="row m-2">
                    {!! Form::open(['route'=>'add.item.price', 'class'=>"form-inline"]) !!}
                    {{ Form::hidden('item_id', $item->id) }}
                    <div class="form-group {{ $errors->has('min_quantity') ? 'has-error' : '' }}">
                        {!! Form::label('Min quantity:') !!}
                        {!! Form::number('min_quantity', old('min_quantity'), ['class'=>'form-control', 'placeholder'=>'Enter min quantity']) !!}
                    </div>
                    <div class="form-group {{ $errors->has('max_quantity') ? 'has-error' : '' }}">
                        {!! Form::label('Max quantity:') !!}
                        {!! Form::number('max_quantity', old('max_quantity'), ['class'=>'form-control', 'placeholder'=>'Enter max quantity']) !!}
                    </div>
                    <div class="form-group {{ $errors->has('unit_price') ? 'has-error' : '' }}">
                        {!! Form::label('Unit price:') !!}
                        {!! Form::number('unit_price', old('unit_price'), ['class'=>'form-control', 'placeholder'=>'Enter unit price']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success">Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Price check</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route'=>'item.price.cal', 'class'=>"form-inline"]) !!}
                    <div class="form-group {{ $errors->has('min_quantity') ? 'has-error' : '' }}">
                        {!! Form::label('Quantity:') !!}
                        {!! Form::number('quantity', old('quantity'), ['class'=>'form-control', 'id' => 'itemCalQty', 'placeholder'=>'Enter quantity']) !!}
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success" onclick="calItemPrice({{$item->id}})">Calculate</button>
                    </div>
                    {!! Form::close() !!}
                    <div class=" mt-3 cal-div d-none">
                        <div class="form-group">
                            <label>Unit Price</label>
                            <input type="text" id="calUnitPrice" class="form-control" value="" readonly>
                        </div>
                        <div class="form-group">
                            <label>Total Price</label>
                            <input type="text" id="calTotalPrice" class="form-control" value="" readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection
