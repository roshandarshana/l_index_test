@extends('layout.base')

@section('title', 'Category page')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card m-2">
                <div class="card-header">
                    All Categories
                </div>
                <ul id="tree1">
                    @foreach($categories as $category)
                        <li>
                            <a href="{{route('category', ['id' => $category->id])}}">{{ $category->title }}</a>
                            @if(count($category->childs))
                                @include('layout.include.categoryChild',['childs' => $category->childs])
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card m-2">
                <div class="card-header">
                    Add Category
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>'add.category']) !!}
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            {!! Form::label('Title:') !!}
                            {!! Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=>'Enter Title']) !!}
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                            {!! Form::label('Parent Category:') !!}
                            {!! Form::select('parent_id',$allCategories, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                            <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Add New</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
