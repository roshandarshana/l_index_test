@extends('layout.base')

@section('title', 'Category detail page')

@section('content')
    <h2>{{ucfirst($category->title)}} category detail</h2>
    <div class="row">
        <div class="col-md-4">
            <div class="card m-2">
                <div class="card-header">
                    Category Items
                </div>
                <div class="list-group">
                    @foreach($category->items as $item)
                    <a href="{{route('item', ['id' => $item->id])}}" class="list-group-item list-group-item-action">
                        {{ $item->name }}
                        <img src="{{url('/item_images/'.$item->images()->first()->file_name)}}" class="img-thumbnail w-25 pull-right" />
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card m-2">
                <div class="card-header">
                    Edit Category
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>'edit.category']) !!}
                    {{ Form::hidden('category_id', $category->id) }}
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            {!! Form::label('Title:') !!}
                            {!! Form::text('title', old('title', $category->title), ['class'=>'form-control', 'placeholder'=>'Enter Title']) !!}
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                            {!! Form::label('Parent Category:') !!}
                            {!! Form::select('parent_id',$allCategories, old('parent_id', $category->parent_id), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                            <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Edit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
