<ul>
    @foreach($childs as $child)
        <li>
            <a href="{{route('category', ['id' => $child->id])}}">{{ $child->title }}</a>
            @if(count($child->childs))
                @include('layout.include.categoryChild',['childs' => $child->childs])
            @endif
        </li>
    @endforeach
</ul>
