<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\CategoryController;
use \App\Http\Controllers\ItemsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CategoryController::class, 'getAllCategoryPage'])->name('categories');
Route::get('/category/{id}', [CategoryController::class, 'getCategoryPage'])->name('category');
Route::post('/add-category', [CategoryController::class, 'addCategory'])->name('add.category');
Route::post('/edit-category', [CategoryController::class, 'editCategory'])->name('edit.category');

Route::get('/items', [ItemsController::class, 'getAllItemsPage'])->name('items');
Route::get('/item/{id}', [ItemsController::class, 'getItemPage'])->name('item');
Route::post('/add-item', [ItemsController::class, 'addItem'])->name('add.item');
Route::post('/edit-item', [ItemsController::class, 'editItem'])->name('edit.item');
Route::post('/add-item-price', [ItemsController::class, 'addItemPrice'])->name('add.item.price');
Route::post('/edit-item-price', [ItemsController::class, 'editItemPrice'])->name('edit.item.price');
Route::get('/item-price-cal', [ItemsController::class, 'calItemPrice'])->name('item.price.cal');
Route::get('/remove-item-image/{id}', [ItemsController::class, 'removeItemImage'])->name('remove.item.image');



//Route::get('/', function () {
//    return view('welcome');
//});
