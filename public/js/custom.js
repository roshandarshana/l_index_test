function removeImage(imgId) {
    if (confirm('Are you sure ?')) {
        $.ajax({
            type: "GET",
            url: "/remove-item-image/"+imgId,
            success: function(data){
                if (data.type == 'success') {
                    location.reload();
                }
                console.log(data.type);
            }
        });
    }
}

function calItemPrice(itemId) {
    qty = $('#itemCalQty').val();
    if (qty > 0) {
        $.ajax({
            type: "GET",
            url: "/item-price-cal",
            data: {
              'itemId' : itemId,
              'qty' : qty,
            },
            success: function(data){
                if (data.type == 'success') {
                    $('#calUnitPrice').val(data.unit_price);
                    $('#calTotalPrice').val(data.total_price);
                    $('.cal-div').removeClass('d-none');
                }
            }
        });
    } else {
        $('.cal-div').addClass('d-none');
    }
}
