<?php

namespace App\Http\Controllers;

use App\Models\ItemImage;
use App\Models\ItemPrice;
use DB;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function getAllItemsPage()
    {
        return view('layout.pages.all_items')
            ->with('items', Item::all())
            ->with('allCategories', Category::pluck('title','id')->all());
    }

    public function getItemPage($id)
    {
        $item = Item::find($id);
        if (empty($item)) {
            return back()->with('danger', 'Could not found Category.');
        }
        return view('layout.pages.item_details')
            ->with('item', $item)
            ->with('allCategories', Category::pluck('title','id')->all());
    }

    public function addItem(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'base_price' => 'required',
            "category_id"    => "required|array",
            "category_id.*"  => "required",
            'images' => 'required',
            'images.*' => 'mimes:jpeg,jpg,png'
        ]);

        try {
            $item = new Item();
            DB::transaction(function () use ($request, $item) {
                $item->name = $request->get('name');
                $item->base_price = $request->get('base_price');
                $item->save();
                $item->categories()->sync($request->get('category_id'));
                $item->save();

                if($request->hasfile('images')) {
                    foreach($request->file('images') as $file) {
                        $this->uploadImage($file, $item->id);
                    }
                }
            });
        } catch (Exception $e) {
            \Log::error($e->getMessage() . ' - ItemController - ' . __LINE__);
            return back()->with('danger', 'Something went wrong.');
        }
        return redirect()->route('item', ['id' => $item->id])->with('success', 'New Item added successfully.');
    }

    public function editItem(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'base_price' => 'required',
            'item_id' => 'required|exists:items,id',
            "category_id"    => "required|array",
            "category_id.*"  => "required"
        ]);

        try {
            DB::transaction(function () use ($request) {
                $item = Item::find($request->get('item_id'));
                $item->name = $request->get('name');
                $item->base_price = $request->get('base_price');
                $item->save();
                $item->categories()->sync($request->get('category_id'));
                $item->save();

                if($request->hasfile('images')) {
                    foreach($request->file('images') as $file) {
                        $this->uploadImage($file, $item->id);
                    }
                }
            });
        } catch (Exception $e) {
            \Log::error($e->getMessage() . ' - ItemController - ' . __LINE__);
            return back()->with('danger', 'Something went wrong.');
        }
        return back()->with('success', 'New Item Updated successfully.');
    }

    private function uploadImage($file, $itemId)
    {
        $name = time().rand(0, 100000).'.'.$file->extension();
        $file->move(public_path().'/item_images/', $name);
        $data = [
            'file_name' => $name,
            'item_id' => $itemId,
        ];
        ItemImage::create($data);
    }

    public function removeItemImage($imageId) {
        $image = ItemImage::find($imageId);
        if (empty($image)) {
            return response()->json(['type' => 'danger', 'message' => 'Image not found'], 404);
        }
        $image->delete();
        return response()->json(['type' => 'success', 'message' => 'Updated successfully'], 200);
    }

    public function addItemPrice(Request $request) {
        $this->validate($request, [
            'min_quantity' => 'required',
            'max_quantity' => 'required',
            'item_id' => 'required|exists:items,id',
            "unit_price"  => "required"
        ]);
        try {
            $itemPrice = new ItemPrice();
            DB::transaction(function () use ($request, $itemPrice) {
                $itemPrice->min_quantity = $request->get('min_quantity');
                $itemPrice->max_quantity = $request->get('max_quantity');
                $itemPrice->unit_price = $request->get('unit_price');
                $itemPrice->item_id = $request->get('item_id');
                $itemPrice->save();
            });
        } catch (Exception $e) {
            \Log::error($e->getMessage() . ' - ItemController - ' . __LINE__);
            return back()->with('danger', 'Something went wrong.');
        }
        return back()->with('success', 'New item price added successfully.');
    }

    public function calItemPrice(Request $request) {
        $itemId = $request->get('itemId');
        $quantity = $request->get('qty');
        $item = Item::find($itemId);
        $prices = $item->prices;

        $price = $prices->filter(function ($price) use ($quantity) {
           return $quantity >= $price->min_quantity && $quantity < $price->max_quantity;
        })->first();

        $price = empty($price) ? $item->base_price : $price->unit_price;

        return response()->json(['type' => 'success', 'unit_price' => $price, 'total_price' => $price * $quantity], 200);
    }
}
