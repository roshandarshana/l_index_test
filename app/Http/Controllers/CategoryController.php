<?php

namespace App\Http\Controllers;

use Log;
use Exception;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getCategoryPage($id)
    {
        $category = Category::find($id);
        if (empty($category)) {
            return back()->with('danger', 'Could not found Category.');
        }
        return view('layout.pages.category_details')
            ->with('category', $category)
            ->with('allCategories', Category::pluck('title','id')->all());
    }

    public function getAllCategoryPage()
    {
        return view('layout.pages.all_category')
            ->with('categories', Category::where('parent_id', '=', 0)->get())
            ->with('allCategories', Category::pluck('title','id')->all());
    }

    public function addCategory(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];

        try {
            Category::create($input);
        } catch (Exception $e) {
            \Log::error($e->getMessage() . ' - CategoryController - ' . __LINE__);
            return back()->with('danger', 'Something went wrong.');
        }
        return back()->with('success', 'New Category added successfully.');
    }

    public function editCategory(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
        ]);
        $parent_id = $request->get('parent_id');

        try {
            $category = Category::find($request->get('category_id'));
            $category->title = $request->get('title');
            $category->parent_id = empty($parent_id) ? 0 : $parent_id;
            $category->save();
        } catch (Exception $e) {
            \Log::error($e->getMessage() . ' - CategoryController - ' . __LINE__);
            return back()->with('danger', 'Something went wrong.');
        }
        return back()->with('success', 'Category updated successfully.');
    }
}
