<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $fillable = ['name','base_price'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'category_item', 'item_id', 'category_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ItemImage', 'item_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\ItemPrice', 'item_id', 'id');
    }
}
