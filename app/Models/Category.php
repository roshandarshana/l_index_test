<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['title','parent_id'];

    public function childs() {
        return $this->hasMany('App\Models\Category','parent_id','id') ;
    }

    public function items()
    {
        return $this->belongsToMany('App\Models\Item', 'category_item', 'category_id', 'item_id');
    }
}
